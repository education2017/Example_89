package hr.apps.mosaic.example_89;

/**
 * Created by Stray on 17.9.2017..
 */

public class SqlQueries {

	static class CREATE_TABLES{
		static String CREATE_TABLE_USERS = "CREATE TABLE " + Schema.TABLE_USERS + " ("
				+ Schema.USERS_ID + " INTEGER PRIMARY KEY, "
				+ Schema.USERS_NAME + " TEXT UNIQUE NOT NULL, "
				+ Schema.USERS_AGE + " INTEGER, "
				+ Schema.USERS_POINTS + " INTEGER);";
	}

	static class DROP_TABLES{
		static String DROP_TABLE_USERS = "DROP TABLE IF EXISTS " + Schema.TABLE_USERS;
	}
}
