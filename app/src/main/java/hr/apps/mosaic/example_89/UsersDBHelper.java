package hr.apps.mosaic.example_89;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stray on 17.9.2017..
 */

public class UsersDBHelper extends SQLiteOpenHelper {

	private static UsersDBHelper mUsersDBHelper = null;

	private UsersDBHelper(Context context){
		super(context,Schema.DATABASE_NAME,null,Schema.DATABASE_VERSION);
	}

	public static synchronized UsersDBHelper getInstance(Context context){

		if(mUsersDBHelper == null){
			context = context.getApplicationContext();
			mUsersDBHelper = new UsersDBHelper(context);
		}
		return mUsersDBHelper;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(SqlQueries.CREATE_TABLES.CREATE_TABLE_USERS);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(SqlQueries.DROP_TABLES.DROP_TABLE_USERS);
		this.onCreate(db);
	}

	public void insertUser(User user){
		ContentValues values = new ContentValues();
		values.put(Schema.USERS_NAME, user.getName());
		values.put(Schema.USERS_AGE, user.getAge());
		values.put(Schema.USERS_POINTS, user.getPoints());
		SQLiteDatabase database = getWritableDatabase();
		database.insert(Schema.TABLE_USERS,null,values);
	}

	public List<User> retrieveUsers(){
		SQLiteDatabase database = getWritableDatabase();
		String[] columns = new String[]{Schema.USERS_NAME, Schema.USERS_AGE, Schema.USERS_POINTS};
		Cursor result = database.query(Schema.TABLE_USERS,columns,null,null,null,null,null);
		return parseUsersFrom(result);
	}

	public void removeUser(User user){
		SQLiteDatabase database = getWritableDatabase();
		String[] whereCondition = new String[]{user.getName()};
		database.delete(Schema.TABLE_USERS, Schema.USERS_NAME+"=?",whereCondition);
	}

	private List<User> parseUsersFrom(Cursor result) {
		List<User> users = new ArrayList<>();
		if(result.moveToFirst()){
			do{
				String name = result.getString(result.getColumnIndex(Schema.USERS_NAME));
				int age = result.getInt(result.getColumnIndex(Schema.USERS_AGE));
				int points = result.getInt(result.getColumnIndex(Schema.USERS_POINTS));
				User user = new User(name, age, points);
				users.add(user);
			}while(result.moveToNext());
			result.close();
		}
		return users;
	}

	public int getPointsInfo(User user) {
		SQLiteDatabase database = getWritableDatabase();
		String[] columns = new String[]{Schema.USERS_POINTS};
		String[] whereClause = new String[]{user.getName()};
		Cursor result = database.query(Schema.TABLE_USERS,columns,Schema.USERS_NAME+"=?",
				whereClause,null,null,Schema.USERS_POINTS);
		if(result.moveToFirst()){
			int points = result.getInt(result.getColumnIndex(Schema.USERS_POINTS));
			return points;
		}
		return -1;
	}
}
