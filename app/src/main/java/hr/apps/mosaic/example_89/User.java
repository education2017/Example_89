package hr.apps.mosaic.example_89;

/**
 * Created by Stray on 17.9.2017..
 */

class User {
	String mName;
	int mAge;
	int mPoints;

	public User() {
	}

	public User(String name, int age, int points) {
		mName = name;
		mAge = age;
		mPoints = points;
	}

	public String getName() {
		return mName;
	}

	public int getAge() {
		return mAge;
	}

	public int getPoints() {
		return mPoints;
	}
}
