package hr.apps.mosaic.example_89;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.OnItemLongClick;

public class MainActivity extends Activity {

	private static String TAG = "MyDBApp";


	@BindView(R.id.bAddUser) Button bAddUser;
	@BindView(R.id.lvUsers)	ListView lvUsers;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ButterKnife.bind(this);
		UserAdapter adapter = new UserAdapter(this);
		this.lvUsers.setAdapter(adapter);
	}

	@OnClick(R.id.bAddUser)
	public void addUser(){
		Random rnd = RandomGenerator.getInstance();
		String name = rnd.nextInt(1000000) + "_User";
		int age = rnd.nextInt(99);
		int points = rnd.nextInt(1000);
		User user = new User(name, age, points);

		UserAdapter adapter = (UserAdapter) lvUsers.getAdapter();
		adapter.insertUser(user);
	}

	@OnItemLongClick(R.id.lvUsers)
	public boolean onItemLongClick(int position){
		UserAdapter adapter = (UserAdapter) lvUsers.getAdapter();
		adapter.removeUser(position);
		return false;
	}

	@OnItemClick(R.id.lvUsers)
	public void onItemClick(int position){
		UserAdapter adapter = (UserAdapter) lvUsers.getAdapter();
		this.displayToast(adapter.getUserPoints(position));
	}

	private void displayToast(String userPoints) {
		Toast.makeText(this,userPoints,Toast.LENGTH_SHORT).show();
	}
}
