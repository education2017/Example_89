package hr.apps.mosaic.example_89;

import java.util.Random;

/**
 * Created by Stray on 17.9.2017..
 */

public class RandomGenerator {
	private static Random mRandomGenerator;

	private RandomGenerator(){
		mRandomGenerator = new Random();
	}

	public static synchronized Random getInstance(){
		if(mRandomGenerator == null){
			mRandomGenerator = new Random();
		}
		return mRandomGenerator;
	}
}
