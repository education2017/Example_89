package hr.apps.mosaic.example_89;

import android.content.ContentValues;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Stray on 17.9.2017..
 */

public class UserAdapter extends BaseAdapter{

	private List<User> mUserList;
	private UsersDBHelper mUsersDBHelper;

	public UserAdapter(Context context) {
		mUsersDBHelper = UsersDBHelper.getInstance(context);
		mUserList = mUsersDBHelper.retrieveUsers();
	}

	@Override
	public int getCount() {
		return this.mUserList.size();
	}

	@Override
	public Object getItem(int position) {
		return this.mUserList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		UserViewHolder holder;
		if(convertView==null){
			convertView = LayoutInflater.from(parent.getContext())
					.inflate(R.layout.list_item_user, parent, false);
			holder = new UserViewHolder(convertView);
			convertView.setTag(holder);
		}else{
			holder = (UserViewHolder) convertView.getTag();
		}

		User user = this.mUserList.get(position);
		holder.tvUserName.setText(user.getName());
		holder.tvUserAge.setText(String.valueOf(user.getAge()));
		holder.tvUserPoints.setText(String.valueOf(user.getPoints()));

		return convertView;
	}

	static class UserViewHolder{
		@BindView(R.id.tvUserName) TextView tvUserName;
		@BindView(R.id.tvUserAge) TextView tvUserAge;
		@BindView(R.id.tvUserPoints) TextView tvUserPoints;

		public UserViewHolder(View view) {
			ButterKnife.bind(this,view);
		}
	}

	public void insertUser(User user){
		this.mUsersDBHelper.insertUser(user);
		this.refreshData();
	}

	public void removeUser(int position){
		this.mUsersDBHelper.removeUser(this.mUserList.get(position));
		this.refreshData();
	}

	public String getUserPoints(int position) {
		User user = this.mUserList.get(position);
		int points = this.mUsersDBHelper.getPointsInfo(user);
		return user.getName() + ": " + points;
	}

	public void refreshData(){
		this.mUserList = this.mUsersDBHelper.retrieveUsers();
		this.notifyDataSetChanged();
	}
}
